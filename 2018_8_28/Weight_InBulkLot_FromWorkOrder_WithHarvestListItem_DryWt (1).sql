﻿
-- Weight in 'Bulk_lots' from 'Work_orders' # All  or /601 / 626 
-- [ From 'work_orders' -> 'bulk_lots' as a DRY Weight  i.e, 1880.4g (and bulk_lots.id = 601)]

select bulk_lots.id as bulk_lot_ID,  harvests.id as Harvest_ID, work_orders.id as Work_order_ID, 
	work_orders.work_order_id, work_orders.list_item_id, list_items.name,list_items.tag,
	sum (w2.weight) as BulkLot_wieght_from_WorkOrder
from weight_events as w1 
	JOIN weight_events as w2 ON  w2.id = w1.next_id	     -- w1.next_id => bulk_lots.id  [ From 'bulk_lots' -> 'work_orders' ]                          
	JOIN work_orders ON work_orders.id = w1.owner_id  --and work_orders.list_item_id = 37 
	JOIN bulk_lots ON bulk_lots.id = w2.owner_id	and bulk_lots.list_item_id = 6	
	JOIN harvests ON harvests.bulk_lot_id = bulk_lots.id  	
	JOIN list_items ON list_items.id =  work_orders.list_item_id    
	
	
where  w1.owner_type='WorkOrder' and w2.owner_type = 'BulkLot'
	and bulk_lots.id = 601   -- OR --
	
	 	
group by  bulk_lots.id , harvests.id, work_orders.id, list_items.name, list_items.tag