﻿-- Weight in 'Work_orders' from 'Bulk_lots' or Harvests or Batches(Including Plants)# All  or /601 / 106 
-- [ From 'bulk_lots' -> 'work_orders' as a *? Weight  i.e, 18880.4g (and bulk_lots.id = 601)]

select bulk_lots.id as bulk_lot_ID, harvests.id as Harvest_ID, harvests.batch_id, plants.id as Plant_id, 
	sum (w2.weight) as WorkOrder_wieght_from_BulkLot
from weight_events as w1 
	JOIN weight_events as w2 ON  w2.id = w1.next_id	     -- w1.next_id => bulk_lots.id  [ From 'bulk_lots' -> 'work_orders' ]                          
	JOIN bulk_lots ON bulk_lots.id = w1.owner_id		-- w1-->bulk_lots & w2-->work_orders
	JOIN harvests ON harvests.bulk_lot_id = bulk_lots.id  	     
	JOIN plants ON plants.batch_id = harvests.batch_id
	
where  w1.owner_type='BulkLot' and w2.owner_type = 'WorkOrder'
	and bulk_lots.id = 601   -- OR --
	 --and w1.owner_id = 601
	
group by harvests.batch_id, harvests.bulk_lot_id ,plants.id, harvests.id, bulk_lots.id

order by harvest_id