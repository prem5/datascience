﻿-- Weight in Bulk_lots from Harvests or Batches # All  or /601 / 626
-- [ From 'harvests' -> 'bulk_lots' as a Fresh Weight] weight=1958g (In bulk_lots = 601)

select  harvests.bulk_lot_id, harvests.id as Harvest_ID, harvests.batch_id,
	sum (w2.weight) as Harvested_wieght_BATCH
from weight_events as w1 
	JOIN weight_events as w2 ON  w2.id = w1.next_id	     -- w1.next_id => bulk_lots.id  [ From 'harvests' -> 'bulk_lots' ]                          
	JOIN harvests ON harvests.id = w1.owner_id  	     -- w1-->harvests & w2-->bulk_lots
	
where  w1.owner_type='Harvest' and w2.owner_type = 'BulkLot'
	--and harvests.id= 8
	-- and harvests.bulk_lot_id = 601   -- OR --
	 and w2.owner_id = 601
	
group by 
	grouping sets(
	(harvests.batch_id, harvests.bulk_lot_id , harvests.id),
	()
	)


order by harvest_id
