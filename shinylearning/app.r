install.packages("shiny")
#install.packages('rsconnect')
#install.packages("devtools")

library(shiny)
#library(devloots)

#ui <- fluidPage("Hello World")

ui <- fluidPage(
  sliderInput(inputId = "num",
              label = "Choose a number",
              value = 25, min = 1, max = 100
              ),
  plotOutput("hist")
)



server <- function(input,output){
  output$hist <- renderPlot({hist(rnorm(input$num))})
}
shinyApp(ui = ui, server = server)


