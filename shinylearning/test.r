#install.packages("shiny")
#install.packages('rsconnect')

library(shiny)

#ui <- fluidPage("Hello World")

ui <- fluidPage(
  plotOutput("hist"),
  sliderInput(inputId = "num12",
              label = "Choose a number",
              value = 25, min = 1, max = 100
              )
  
)
server <- function(input,output){
  output$hist <- renderPlot({hist(rnorm(input$num12))})
}
shinyApp(ui = ui, server = server)


