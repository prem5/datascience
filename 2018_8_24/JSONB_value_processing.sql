﻿-- Q1 > JSONB values processing i.e,  'terpene_profile' from lab_reports table.
SELECT q.id, d.key, d.value
            FROM lab_reports q
            JOIN jsonb_each(q.terpene_profile) d ON true
            ORDER BY 1, 2;


-- Q2 > JSONB values processing i.e,  'user_data' from ample_user_trackings table.
select * from ample_user_trackings


SELECT q.id, d.key, d.value
FROM ample_user_trackings q JOIN  jsonb_each(q.user_data) d ON true
ORDER BY 1, 2       


SELECT q.id, d.key, d.value
FROM ample_user_trackings q JOIN  jsonb_each_text(q.user_data) d ON true
ORDER BY 1, 2       


SELECT q.id, d.key, d.value
FROM ample_user_trackings q JOIN  jsonb_each_text(q.user_data) d ON true
ORDER BY 2,1     

-- Error --
SELECT q.id, d.key, d.value
FROM ample_user_trackings q JOIN  jsonb_to_recordset(q.user_data) d ON true
ORDER BY 1, 2   