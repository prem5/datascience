﻿-- Weight from Batch # 106
select  harvests.bulk_lot_id, harvests.batch_id, sum (w2.weight) as Harvested_wieght_BATCH
from weight_events as w1 
	JOIN weight_events as w2 ON w1.next_id = w2.id and w1.owner_type='Harvest' -- w1.next_id => bulk_lots.id
	JOIN bulk_lots ON bulk_lots.id = w1.owner_id                               -- 
	JOIN harvests ON harvests.id = w1.owner_id -- and harvests.batch_id = 47

group by harvests.batch_id, harvests.bulk_lot_id 

