﻿select batch_id, sum(harvested_weight) 		-- , in_prop, in_veg, in_flower, in_harvest, 
						-- birthday, prop_date, veg_date, flower_date, harvest_date, 
						-- prop_duration_days, veg_duration_days, flower_duration_days, 
						-- veg_date - birthday as pop_time, flower_date -  veg_date as veg_time, harvest_date - flower_date as flower_time
from plants
where in_harvest = TRUE and in_prop = false and in_veg = false and in_flower = false and (harvested_weight)>0
-- and batch_id = 47
group by batch_id -- , in_harvest, in_prop, in_veg, in_flower, birthday, prop_date, veg_date, flower_date, harvest_date, 
		     -- prop_duration_days, veg_duration_days, flower_duration_days
order by batch_id 