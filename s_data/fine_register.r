install.packages("sqldf")
library(sqldf)
prescriptions = read.csv('prescriptions.csv')

pre_filtered <- Filter(function(x)!all(is.na(x)),prescriptions)
str(pre_filtered)
columns = colnames(pre_filtered)
View(columns)
x = sqldf("select * from pre_filtered limit 5 ")
