#install.packages("sqldf")
library(sqldf)
clients = read.csv("clients_updated.csv")
registrations = read.csv("registrations.csv")
prescriptions = read.csv("prescriptions.csv")
orders = read.csv("orders.csv")
order_items = read.csv("order_items.csv")
prescription_periods = read.csv("prescription_periods_updated.csv")

client_info = sqldf("select clients.id as id,clients.referral_code, clients.migrated_id, registrations.first_name as first_name ,registrations.middle_name as middle_name, registrations.last_name as last_name, registrations.province, registrations.gender, registrations.date_of_birth from registrations inner join clients on clients.id = registrations.client_id order by clients.id limit 10 ")
client_pres = sqldf("select * from prescriptions where prescriptions.thc_limit_high > 0 or  prescriptions.thc_limit_low > 0 or  cbd_limit_low > 0 or  cbd_limit_high > 0")
thc_limit_high_count = sqldf("select prescriptions.id, prescriptions.thc_limit_high , prescriptions.client_id from prescriptions where prescriptions.thc_limit_high > 0 order by client_id")

# client  prescrition count from prescriptions table 
prescription_count_from_prescriptions = sqldf("select prescriptions.id as prescription_id  , prescriptions.client_id, count(*) as prescription_count from prescriptions group by client_id")


# client  prescrition count from prescription_periods table 
prescription_count_periods = sqldf("select * , count(*) as period_count from prescription_periods group by prescription_id ")
prescriptions_max = sqldf("select prescriptions.id, clients.id as client_id , MIN(script_start) as script_start, MAX(script_end) as  script_end , SUM(script_end - script_start) as duration from prescriptions join clients on clients.id = prescriptions.client_id  group by clients.id   order by duration desc ")

prescription_count = sqldf("select prescription_count_from_prescriptions.prescription_id as p_c_id, prescription_count_from_prescriptions.prescription_count as p_c_count , prescription_count_periods.* from prescription_count_from_prescriptions left join prescription_count_periods on prescription_count_from_prescriptions.prescription_id = prescription_count_periods.prescription_id ")



thc_limit_low_count = sqldf("select * from prescriptions where prescriptions.thc_limit_low > 0")
cbd_limit_low_count = sqldf("select * from prescriptions where prescriptions.cbd_limit_low > 0")
cbd_limit_high_count = sqldf("select * from prescriptions where prescriptions.cbd_limit_high > 0")
prescriptions$cbd_limit_low

# 2018_07_11
prescription_gram = sqldf("select id, number_of_grams from prescriptions ")

prescription_new = sqldf("select * from prescription_periods")
prescription_new$start_date = as.Date(prescription_new$start_date, format = "%m/%d/%Y")

prescription_new$end_date = as.Date(prescription_new$end_date, format = "%m/%d/%Y")

prescription_new$days = prescription_new$end_date - prescription_new$start_date
prescription_total_days = sqldf("select *  from prescription_new")

prescription_period_days = sqldf(" select *  from prescription_new order by prescription_id ASC ")
