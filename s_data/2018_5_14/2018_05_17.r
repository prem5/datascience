library(sqldf)
clients = read.csv("clients_updated.csv")
registrations = read.csv("registrations.csv")
prescriptions = read.csv("prescriptions.csv")
orders = read.csv("orders.csv")
order_items = read.csv("order_items.csv")
skus = read.csv("skus_updated_rf2.csv")

prescriptions_range = sqldf("select prescriptions.id, clients.id as client_id , script_start , script_end , (script_end - script_start) as rang from prescriptions join clients on clients.id = prescriptions.client_id    where clients.id = 4231")

str(prescriptions_range$script_start)

prescriptions_max = sqldf("select prescriptions.id, clients.id as client_id , MIN(script_start) as script_start, MAX(script_end) as  script_end , SUM(script_end - script_start) as duration from prescriptions join clients on clients.id = prescriptions.client_id  group by clients.id   order by duration desc ")

prescriptions_max = sqldf("select prescriptions.id, clients.id as client_id , MIN(script_start) as script_start, MAX(script_end) as  script_end , SUM(script_end - script_start) as duration from prescriptions join clients on clients.id = prescriptions.client_id  group by clients.id   order by duration desc ")

prescriptions_not_expired = subset(prescriptions_max,script_end > Sys.Date())

str(prescriptions_range_max_min$start)
order_completed = sqldf("select * from orders where status = 'Order Delivered'")
order_completed$delivered_on <-as.Date(order_completed$delivered_on,format ="%m/%d/%y")
order_completed$delivered_on <- order_completed$delivered_on 
order_completed$delivered_on <-format(order_completed$delivered_on,format ="%m/%d/%Y")
order_not_delivered = sqldf("select * from order_completed where  delivered_on is null")

prescriptions$script_start<-as.Date(prescriptions$script_start,format ="%m/%d/%Y")
prescriptions$script_end<-as.Date(prescriptions$script_end,format ="%m/%d/%Y")

remain_pres = sqldf("select prescriptions.id, clients.id as client_id ,script_start , script_end , (script_end - script_start) as rang from prescriptions join clients on clients.id = prescriptions.client_id    order by clients.id  ")

remain_pres1= subset(remain_pres, script_end > Sys.Date())
str(remain_pres$script_start)

pres_orders = sqldf("select orders.id, orders.purchased_on,prescriptions.script_start,prescriptions.script_end  from orders join prescriptions on orders.prescription_id = prescriptions.id")

prescriptions_no_order = sqldf("select prescriptions.* from prescriptions left outer join orders on orders.prescription_id = prescriptions.id where prescriptions.id is null")

str(order_completed$delivered_on)
