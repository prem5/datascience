install.packages("sqldf")
library(sqldf)
clients = read.csv("clients_updated.csv")
registrations = read.csv("registrations.csv")
prescriptions = read.csv("prescriptions.csv")
orders = read.csv("orders.csv")
order_items = read.csv("order_items.csv")
client_info = sqldf("select clients.*, registrations.first_name ,registrations.middle_name, registrations.last_name from registrations inner join clients on clients.id = registrations.client_id order by clients.id limit 10 ")
prescription_count = sqldf("select client_id, count(id) as prescription_count from prescriptions group by client_id order by client_id")

max_prescription_list = sqldf("select client_id, count(id)  as prescription_count from prescriptions group by client_id order by count(id) desc")


max_prescription = sqldf("select prescription_count.*  from prescription_count order by prescription_count desc ")


prescription_oder = sqldf("select  prescriptions.client_id, prescriptions.id as prescription_id, count(orders.id)  as order_count  from prescriptions inner join orders on prescriptions.id = orders.prescription_id group by prescriptions.id order by order_count desc ")

order

pres_order = sqldf("select orders.id , orders.client_id, prescriptions.client_id as c_id , orders.purchased_on ,orders.delivered_on from orders left join prescriptions on orders.prescription_id = prescriptions.id where orders.status = 'Order Delivered' order by orders.purchased_on ")


pre_order= sqldf("select prescriptions.id,prescriptions.client_id as client_id,sum(number_of_grams) as number_of_grams , count(orders.id) as order_count, sum(invoice_total_grams) 
                 as gram  from orders left join prescriptions on orders.prescription_id = prescriptions.id group by prescriptions.id order by client_id")
pre_order_client= sqldf("select s1.client_id as client_id,registrations.first_name,registrations.middle_name,registrations.last_name, count(s1.client_id) as prescription_count , sum(order_count) as order_count, sum(gram) as total_gram, sum(s1.number_of_grams) as prescribe_gram 
from (select prescriptions.id,sum(number_of_grams) as number_of_grams,prescriptions.client_id as client_id, count(orders.id) as order_count, sum(invoice_total_grams) 
                 as gram  from orders left join prescriptions on orders.prescription_id = prescriptions.id group by prescriptions.id order by client_id) as s1 join registrations on registrations.client_id = s1.client_id group by s1.client_id order by total_gram desc")
prescriptions$number_of_grams
