SELECT
Orders.source_name AS "Patient Name",
clients.client_id AS "Client ID",
registrations.province AS "Province",
clients.referral_code AS "Referral Code Registration", --#change in schema May 2018 = registrations.referral_code AS "Referral Code Registration"
prescriptions.referral_code AS "Refferal Code Prescription",
Orders.id as "Order Number",
products.name AS product_name,
products.name || '(' || skus.name || ')' AS sku_name,
(SELECT array_agg(bulk_lots.name)
   FROM bulk_lots
   WHERE bulk_lots.id = ANY(
                              (SELECT array
                                 (SELECT bottles.bulk_lot_id
                                  FROM bottles
                                  WHERE bottles.packing_order_id =
                                      (SELECT packing_orders.id
                                       FROM packing_orders
                                       WHERE packing_orders.order_id = order_items.order_id)
                                    AND bottles.sku_id = order_items.sku_id))::integer[])) AS "Release Lot",
                                   
skus.unit_grams * order_items.quantity AS "Grams Ordered",
ROUND(((order_items.original_unit_price / 100.00) * order_items.quantity)::numeric, 2) AS "Order Item Gross Sale",
(SELECT SUM(discounts.dollar_amount)
   FROM discounts
   WHERE discounts.order_id = orders.id) AS "Discount Order",
ROUND((order_items.invoice_discount / 100.00)::numeric, 2) AS "Discount Order Item",
(ROUND(((order_items.original_unit_price / 100.00) * order_items.quantity)::numeric, 2)-ROUND((order_items.invoice_discount / 100.00)::numeric, 2)) as "Order Item Net Sale",
--CASE
	--WHEN (skus.unit_grams * order_items.quantity) IS NOT NULL THEN ((ROUND(((order_items.original_unit_price / 100.00) * order_items.quantity)::numeric, 2)-ROUND((order_items.invoice_discount / 100.00)::numeric, 2))/(skus.unit_grams * order_items.quantity))
	--ELSE NULL
	--END as "Cost Per Gram",
ROUND((orders.invoice_tax / 100.00)::numeric, 2) AS "Tax",
SUM(ROUND((orders.invoice_tax / 100.00)::numeric, 2)) OVER (ORDER by orders.id ASC) as "tax 2",--need to figure out, so doesn't repeat
ROUND((orders.invoice_shipping / 100.00)::numeric, 2) AS "Shipping Revenue",
SUBSTRING(SUBSTRING(orders.shipping_rate FROM ':rate:..d*.d*')FROM 'd.*') AS "Shipping Expense",
product_types.name AS "Product Type",
registrations.knumber AS "K#",
--Order type? Repeat?
 COALESCE(CASE
                    WHEN orders.invoice_payment_method != '' THEN orders.invoice_payment_method
                    ELSE NULL
                END, CASE COALESCE(substring(receipts.response, 'ActiveMerchant.*params.*card_type: ([0-9A-Za-z_]+)'), substring(receipts.response, 'payment_type: ([0-9A-Za-z_]+)'))
                         WHEN 'V' THEN 'Visa'
                         WHEN 'M' THEN 'Mastercard'
                         WHEN 'AX' THEN 'American Express'
                         WHEN 'money_order' THEN 'Money Order'
                         WHEN 'cheque' THEN 'Personal Cheque'
                         WHEN 'cash' THEN 'Cash'
                         WHEN 'etransfer' THEN 'Interac e-Transfer'
                         ELSE COALESCE(substring(receipts.response, 'ActiveMerchant.*params.*card_type: ([0-9A-Za-z_]+)'), substring(receipts.response, 'payment_type: ([0-9A-Za-z_]+)'), '')
                     END) AS "Payment Method",
orders.order_source AS "Order Source",
orders.source_name as "Order Source Name",
orders.shipped_on AS "Shipped On",
(orders.purchased_on AT TIME ZONE 'UTC' AT TIME ZONE 'UTC')::date AS "Purchase Date",
to_char(orders.purchased_on AT TIME ZONE 'UTC' AT TIME ZONE 'UTC', 'YYYY-MM-DD HH24:MI:SS') AS "Purchase Time"
FROM "order_items"
INNER JOIN "orders" ON "orders"."id" = "order_items"."order_id"
INNER JOIN "prescriptions" ON "prescriptions"."id" = "orders"."prescription_id"
INNER JOIN "orders" "orders_order_items_join" ON "orders_order_items_join"."id" = "order_items"."order_id"
INNER JOIN "clients" ON "clients"."id" = "orders_order_items_join"."client_id"
INNER JOIN "registrations" ON "registrations"."client_id" = "clients"."id"
--INNER JOIN "registrations" ON "registrations"."id" = "clients"."active_registration_id" #the schema for referral_code will change by May 14,2018. This will be the new field. 
INNER JOIN "skus" ON "skus"."id" = "order_items"."sku_id"
INNER JOIN "products" ON "products"."id" = "skus"."product_id"
INNER JOIN "product_types" ON "product_types"."id" = "products"."product_type_id"
INNER JOIN "skus" "skus_order_items" ON "skus_order_items"."id" = "order_items"."sku_id"
LEFT OUTER JOIN discounts ON discounts.order_id = orders.id
LEFT JOIN receipts ON receipts.id =
  (SELECT id
   FROM receipts
   WHERE receipts.order_id = orders.id
     AND purchase = TRUE
   ORDER BY id ASC
   LIMIT 1)
WHERE ("orders"."created_at" BETWEEN '2018-01-01 00:00:00.000000' AND '2018-3-15 23:59:59.999999')
  AND "orders"."purchased" = 't'
  AND ("orders"."status" NOT IN ('Order Refunded',
                                 'Order Refused'))
  AND orders.id = 392702
GROUP BY "order_items"."id",
         orders.order_source,
         orders.source_name,
         orders.id,
         clients.client_id,
         clients.referral_code,
         --registrations.referral_code, #the schema for referral_code will change by May 14,2018. This will be the new field. 
         registrations.first_name,
         registrations.last_name,
         registrations.province,
         registrations.knumber,
         receipts.response,
         prescriptions.referral_code,
         products.name,
         skus.name,
         product_types.name,
         product_types.is_cannabis,
         skus.unit_grams
ORDER BY orders.id,
         orders.shipped_on
