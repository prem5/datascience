1. packing_orders_ids =  (SELECT packing_orders.id as id 
                        FROM packing_orders 
                        WHERE packing_orders.order_id = order_items.order_id) ;
                        
2. bulk_lot_ids = SELECT array
                                 (SELECT bottles.bulk_lot_id
                                  FROM bottles
                                  WHERE bottles.packing_order_id =  packing_orders_ids.id
                                    AND bottles.sku_id = order_items.sku_id)::integer[];

3. SELECT array_agg(bulk_lots.name)
  FROM bulk_lots 
  WHERE bulk_lots.id = ANY(bulk_lot_ids ) as "Release Lot";
4. skus.unit_grams * order_items.quantity AS "Grams Ordered";
5. ROUND(((order_items.original_unit_price / 100.00) * order_items.quantity)::numeric, 2) AS "Order Item Gross Sale";

6. (SELECT SUM(discounts.dollar_amount)
   FROM discounts
   WHERE discounts.order_id = orders.id) AS "Discount Order";
   
7. ROUND((order_items.invoice_discount / 100.00)::numeric, 2) AS "Discount Order Item";

8. (ROUND(((order_items.original_unit_price / 100.00) * order_items.quantity)::numeric, 2)-ROUND((order_items.invoice_discount / 100.00)::numeric, 2)) as "Order Item Net Sale";
9. ROUND((orders.invoice_tax / 100.00)::numeric, 2) AS "Tax"
10. SUM(ROUND((orders.invoice_tax / 100.00)::numeric, 2)) OVER (ORDER by orders.id ASC) as "tax 2";
11. ROUND((orders.invoice_shipping / 100.00)::numeric, 2) AS "Shipping Revenue"
12. SUBSTRING(SUBSTRING(orders.shipping_rate FROM ':rate:..d*.d*')FROM 'd.*') AS "Shipping Expense";


13. 

 COALESCE(CASE
                    WHEN orders.invoice_payment_method != '' THEN orders.invoice_payment_method
                    ELSE NULL
                END, CASE COALESCE(substring(receipts.response, 'ActiveMerchant.*params.*card_type: ([0-9A-Za-z_]+)'), substring(receipts.response, 'payment_type: ([0-9A-Za-z_]+)'))
                         WHEN 'V' THEN 'Visa'
                         WHEN 'M' THEN 'Mastercard'
                         WHEN 'AX' THEN 'American Express'
                         WHEN 'money_order' THEN 'Money Order'
                         WHEN 'cheque' THEN 'Personal Cheque'
                         WHEN 'cash' THEN 'Cash'
                         WHEN 'etransfer' THEN 'Interac e-Transfer'
                         ELSE COALESCE(substring(receipts.response, 'ActiveMerchant.*params.*card_type: ([0-9A-Za-z_]+)'), substring(receipts.response, 'payment_type: ([0-9A-Za-z_]+)'), '')
                     END) AS "Payment Method",

                                  
  