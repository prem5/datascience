install.packages("sqldf")
library(sqldf)
product_types = read.csv("product_types.csv")
products = read.csv("products.csv")

order_items = read.csv('order_items3.csv')
skus = read.csv("skus3.csv")

registrations = read.csv("registration3.csv")
clients = read.csv('clients3.csv')

orders = read.csv('orders3.csv')


registered_clients = sqldf("select clients.id as id, registrations.status from clients join registrations on registrations.client_id = clients.id where clients.id in (select client_id from registrations where status = 'Approved')   ")

r_clients_orders = sqldf("select * from orders where id in (select id from registered_clients )")



cannabis_oil_products = sqldf("select products.id , products.name, products.product_type_id from products join product_types on products.product_type_id = product_types.id where product_types.name = 'Cannabis Oil'")

cannabis_skus = sqldf("select * from skus where skus.product_id in  ( select id from  cannabis_oil_products) ")


cannabis_order_items = sqldf("select * from order_items where sku_id in (select id from cannabis_skus)")

r_cannabis_order_items = sqldf("select cannabis_order_items.id, cannabis_order_items.quantity,
                               cannabis_order_items.sku_id, skus.name ,
                              skus.unit_grams,skus.net_volume,skus.net_weight,
                              (cannabis_order_items.quantity * skus.net_volume) as volumn,
                              (cannabis_order_items.quantity * skus.net_weight) as weight
                               from cannabis_order_items join skus on skus.id = cannabis_order_items.sku_id where order_id in (select id from  r_clients_orders )  ")
r_kilograms = sqldf("select sum(weight)/1000 from r_cannabis_order_items")
r_litres = sqldf("select sum(volumn)/1000 from r_cannabis_order_items")

#line 97
order_gram = sqldf("select 
                      order_items.id, order_items.sku_id,order_items.quantity,
                          skus.name ,skus.unit_grams,skus.net_volume,(skus.unit_grams*quantity) as total_grams,(skus.net_volume*quantity) as total_volume, skus.product_id
                  from order_items join skus on skus.id = order_items.sku_id where order_items.id in ( select id from cannabis_order_items )  order by total_grams ASC")

order_gram_average = sqldf("select avg(total_grams) from order_gram")
order_volume_average = sqldf("select avg(total_volume) from order_gram")
#line 103

#Grams: 
all_shipment_size= sqldf("select order_gram.id , order_gram.quantity, order_gram.unit_grams   from order_gram ")

# list of all shipment with repeating row quantity times
all_shipment_size.expanded_gram <- all_shipment_size[rep(row.names(all_shipment_size),all_shipment_size$quantity),3:3]
View(all_shipment_size.expanded_gram)
# give median of shipment size

median(all_shipment_size.expanded_gram)

#Millilitres 
all_shipment_volume = sqldf("select order_gram.id , order_gram.quantity, order_gram.net_volume   from order_gram ")

# list of all shipment with repeating row quantity times
all_shipment_volume.expanded_volume <- all_shipment_volume[rep(row.names(all_shipment_volume),all_shipment_volume$quantity),3:3]
View(all_shipment_volume.expanded_volume)
median(all_shipment_size.expanded_gram)




